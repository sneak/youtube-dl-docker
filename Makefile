export DOCKER_HOST := ssh://root@lstor1

.PHONY: default deploy build_image restart stop run logs

default: deploy

deploy:
	make build_image
	make stop
	make start

build_image:
	docker build -t sneak/ytdlp .

restart:
	make stop
	make start

stop:
	-docker stop sneak-ytdlp
	-docker rm -f sneak-ytdlp

start:
	docker run \
		-d \
		--name sneak-ytdlp \
		-v /srv/lstor1/warez/youtube:/output \
		--restart unless-stopped \
		sneak/ytdlp

logs:
	docker logs -f sneak-ytdlp
