#!/bin/bash

DEST="/output"
INTERVAL="3600" #1h
START_TIME="$(date +%s)"

function listChannels {
    SRC="/etc/videolist.txt"
    grep -Ev "^#|^$" "$SRC" | sed 's/#.*$//'
}

function timeRunning {
    expr $(date +%s) - $START_TIME
}

function main {
    init
    curl https://ipinfo.io
    while true ; do
        doDownload
        echo "finished."
        echo "cleaning up"
        find "$DEST" -type f -iname '*.temp.*' -print -delete
        echo "sleeping $INTERVAL"
        sleep $INTERVAL
    done
}

function init {
    umask 0000
    cd "$DEST"
}

function doDownload {
    for CHANNEL in $(listChannels); do
        echo "downloading $CHANNEL"
        HOME="$DEST" yt-dlp --config-location /etc/youtube-dl.conf $CHANNEL
        sleep 1
    done
}

main

