# youtube-dl-docker

inspired by https://old.reddit.com/r/DataHoarder/comments/c6fh4x/after_hoarding_over_50k_youtube_videos_here_is/

# usage

```
docker run \
    -v /storage/scratch/youtube:/output \
    -e DOWNLOAD_URL=https://www.youtube.com/channel/UCsXVk37bltHxD1rDPwtNM8Q/videos \
    sneak/youtube-dl
```

# See Also

[`bin/download-interesting-channels`](https://github.com/sneak/youtube-dl-docker/blob/master/bin/download-interesting-channels), suitable for run from cron

# Author

Jeffrey Paul <[sneak@sneak.berlin](mailto:sneak@sneak.berlin)>
